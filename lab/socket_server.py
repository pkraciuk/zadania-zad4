#!/usr/bin/env python
# -*- coding: utf-8 -*-

from SocketServer import ThreadingMixIn, TCPServer, BaseRequestHandler
import http_server, socket


class MyHandler(BaseRequestHandler):
    def handle(self):
        http_server.http_serve(self.request)


class MyServer(ThreadingMixIn, TCPServer):
    allow_reuse_address = 1


if __name__ == '__init__':
    server = MyServer(('194.29.175.240', 31001), MyHandler)
    server.serve_forever()