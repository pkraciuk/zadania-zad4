# -*- encoding: utf-8 -*-
import socket
import logging
import os
import time
from daemon import runner
import errno

class App:
    def __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pidfile_path = '/tmp/p1demon.pid'
        self.pidfile_timeout = 5
        self.root_path = ''
        # przygotuj strony bledow
        self.page = "<!DOCTYPE html><html><head><meta charset='utf-8'/></head><body><h1>%s</h1></body></html>"
        self._404 = self.page % '404 - Not found'
        self._400 = self.page % '400 - Bad request'


    # Główny kod aplikacji
    def run(self):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        self.server_address = ('194.29.175.240', 31001)
        #self.server_address = ('localhost', 31007)
        self.server_socket.bind(self.server_address)

        self.server_socket.listen(5)
        while True:
            self.http_serve(self.server_socket)

    # metoda zwracajaca odpowiedz w zaleznosci od typu tresci
    # request_type = typ zadanej tresc
    # content = tresc
    def make_answer(self, request_type, content):
        result = ''
        header = "HTTP/1.1 %s %s \r\n"
        type = "Content-type: %s \r\n"
        content_length = "Content-length: %s\r\n"

        if request_type == 'html':
            result += header % ('200', 'OK')
            result += type % 'text/html'
        elif request_type == '404':
            result += header % ('404', 'Not found')
            result += type % 'text/html'
        elif request_type == 'txt':
            result += header % ('200', 'OK')
            result += type % 'text/plain; charset=UTF-8'
        elif request_type == 'jpg':
            result += header % ('200', 'OK')
            result += type % 'image/jpeg'
        elif request_type == 'png':
            result += header % ('200', 'OK')
            result += type % 'image/png'
        elif request_type == 'listing':
            result += header % ('200', 'OK')
            result += type % 'text/html'
        else:
            result += header % ('400', 'Bad request')
            result += type % 'text/html'

        result += content_length % len(content)
        result += "GMT-Date: %s\r\n" % (time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime()))
        result += "\r\n%s" % content
        return result

    # metoda przygotowujaca listing danego katalogu
    # uri = URI zapytania
    def make_listing(self, uri):
        result = "<!DOCTYPE html><html><head><meta charset='utf-8' /></head><body>"
        result += '<h1>Listing directory: ' + uri + '</h1><ul>'

        # dodaj ukosnik, jesli uri go nie zawiera
        if uri[-1:] != '/':
            uri += '/'

        listing = os.listdir(self.root_path + uri)
        for item in listing:
            file_path = uri + item
            if os.path.isdir(self.root_path + uri + item):
                result += '<li><a href="' + file_path + '/">' + item + '/</a></li>'
            else:
                result += '<li><a href="' + file_path + '">' + item + '</a></li>'

        return result

    # metoda sprawdzajaca czy zadanie jest dozwolone
    # request = pierwsza linia naglowka zadania
    def check_request(self, request):
        header = request.split('\r\n')
        if header.count > 0:
            header = header[0].split(' ')
            if header.count > 0:
                if header[0] == "GET" and header[2].split('/')[0] == "HTTP":
                    return 1
        return 0

    def http_serve(self, server_socket):
        connection, client_address = server_socket.accept()

        try:
            request = connection.recv(1024)
            logger.info('Odebrano polaczenie.')
            if request:
                if self.check_request(request):
                    uri = request.split('\r\n')[0].split(' ')[1]

                    path = uri

                    if os.path.exists(path):
                        if os.path.isdir(path): # sprawdz czy to katalog
                            answer = self.make_answer('listing', self.make_listing(uri))
                            logger.info('Wywolano listowanie katalogu.')
                        else: # zadanie o plik
                            file = open(path, 'rb')

                            if uri[-5:] == '.html':
                                type = 'html'
                                logger.info('Wywolano plik HTML.')
                            elif uri[-4:] == '.txt':
                                type = 'txt'
                                logger.info('Wywolano plik TXT.')
                            elif uri[-4:] == '.jpg':
                                type = 'jpg'
                                logger.info('Wywolano plik JPG.')
                            elif uri[-4:] == '.png':
                                type = 'png'
                                logger.info('Wywolano plik PNG.')
                            else:
                                type = 'txt'
                                logger.info('Wywolano inny plik. Traktuje jako TXT.')
                            answer = self.make_answer(type, file.read())
                            logger.info('Wysylanie odpowiedzi.')
                            file.close()

                        # w przypadku zakonczenia zadania przed wyslaniem calej
                        # zawartosci program rzuci wyjatek Errno32 Broken pipe
                        try:
                            connection.sendall(answer)
                        except IOError as e:
                            if e.errno == errno.EPIPE:
                                logger.error('Nieoczekiwanie zamknieto gniazdo.')
                                connection.close()
                    else:
                        connection.sendall(self.make_answer('404', self._404))
                        logger.info('Nie znaleziono pliku - 404.')
                else:
                    connection.sendall(self.make_answer('400', self._400))
                    logger.error('Nieobslugiwane zadanie - 400 - Bad request.')
        except IOError:
            connection.close()
            logger.error('Nieoczekiwanie zamknieto gniazdo.')
        finally:
            connection.close()
            logger.info('Zamknieto gniazdo.')

# Ustawienie loggera
logger = logging.getLogger("DemonLog")
logger.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler = logging.FileHandler("/tmp/demonp1.log")
handler.setFormatter(formatter)
logger.addHandler(handler)

# Stworzenie i uruchomienie aplikacji jako demona
app = App()
daemon_runner = runner.DaemonRunner(app)
# Zapobiegnięcie zamknięciu pliku logów podczas demonizacji
daemon_runner.daemon_context.files_preserve = [handler.stream]
daemon_runner.do_action()



















